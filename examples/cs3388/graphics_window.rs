use graphics::Color;
use std::fs::File;
use std::io::BufWriter;
use std::mem::swap;
use std::path::Path;

pub struct GraphicsWindow {
    width: u32,
    height: u32,
    data: Vec<u8>,
}

type Pixel = (i32, i32);

#[allow(dead_code)]
impl GraphicsWindow {
    pub fn new(width: u32, height: u32) -> GraphicsWindow {
        GraphicsWindow {
            width,
            height,
            data: vec![0; (width * height * 3) as usize],
        }
    }

    pub fn draw_point(&mut self, point: Pixel, color: Color) {
        if 0 < point.0 && point.0 < self.width as i32 && 0 < point.1 && point.1 < self.height as i32
        {
            let point = (point.0 as usize, point.1 as usize);
            let location = 3 * (point.1 * self.width as usize + point.0);
            for i in 0..3 {
                self.data[location + i] = color[i];
            }
        }
    }

    pub fn draw_line(&mut self, p1: Pixel, p2: Pixel, color: Color) {
        // todo: clipping
        let (mut x1, mut y1) = p1;
        let (mut dx, mut _x) = if p2.0 >= x1 {
            (p2.0 - x1, 1)
        } else {
            (x1 - p2.0, -1)
        };
        let (mut dy, mut _y) = if p2.1 >= y1 {
            (p2.1 - y1, 1)
        } else {
            (y1 - p2.1, -1)
        };
        let flipped;
        if dx < dy {
            swap(&mut x1, &mut y1);
            swap(&mut dx, &mut dy);
            swap(&mut _x, &mut _y);
            flipped = true;
        } else {
            flipped = false;
        }
        self.draw_point(p1, color);
        let mut p_i = 2 * dy - dx;
        for _ in 0..dx {
            if p_i < 0 {
                p_i += 2 * dy;
            } else {
                p_i += 2 * (dy - dx);
                y1 += _y;
            }
            x1 += _x;
            if flipped {
                self.draw_point((y1, x1), color);
            } else {
                self.draw_point((x1, y1), color);
            }
        }
    }

    pub fn fill_polygon(&mut self, points: &[[f64; 2]], color: Color) {
        let n = points.len();
        let mut active = vec![false; n];
        let mut horizontal = vec![false; n];
        let mut min_y = points[0][1] as i32;
        let mut max_y = points[0][1] as i32;
        for point in points {
            let y = point[1] as i32;
            if y < min_y {
                min_y = y;
            }
            if y > max_y {
                max_y = y;
            }
        }
        for i in 0..n {
            horizontal[i] = points[i][1] as i32 == points[(i + 1) % n][1] as i32;
        }
        for y in min_y..max_y {
            let mut intersections = vec![];
            for i in 0..n {
                if !horizontal[i] {
                    active[i] = points[i][1] as i32 <= y && y <= points[(i + 1) % n][1] as i32
                        || points[i][1] as i32 >= y && y >= points[(i + 1) % n][1] as i32
                }
            }
            for i in 0..n {
                if active[i] && !horizontal[i] {
                    if points[i][0] as i32 == points[(i + 1) % n][0] as i32 {
                        intersections.push(points[i][0] as i32);
                    } else {
                        let m = (points[(i + 1) % n][1] as i32 - points[i][1] as i32) as f64
                            / (points[(i + 1) % n][0] as i32 - points[i][0] as i32) as f64;
                        let b = points[i][1].floor() - m * points[i][0].floor();
                        intersections.push(((y as f64 - b) / m as f64).round() as i32);
                    }
                }
            }
            intersections.sort();
            for i in intersections[0]..intersections[intersections.len() - 1] {
                self.draw_point((i, y), color);
            }
        }
    }

    pub fn draw_faces(&mut self, mut face_list: Vec<(f64, Vec<[f64; 2]>, Color)>) {
        face_list.sort_by(|x, y| x.0.partial_cmp(&y.0).unwrap());
        for (_, points, color) in face_list {
            self.fill_polygon(&points, color);
        }
    }

    pub fn draw_polygon(&mut self, points: &[Pixel], color: Color) {
        for i in 0..(points.len() - 1) {
            self.draw_line(points[i], points[i + 1], color);
        }
        self.draw_line(points[points.len() - 1], points[0], color)
    }

    pub fn draw_poly_line(&mut self, points: &[Pixel], color: Color) {
        for i in 0..(points.len() - 1) {
            self.draw_line(points[i], points[i + 1], color);
        }
    }

    pub fn draw_segments(&mut self, segments: &[(Pixel, Pixel)], color: Color) {
        for s in segments {
            self.draw_line(s.0, s.1, color);
        }
    }

    pub fn save_image(&self, name: &str) {
        let file = File::create(Path::new(name)).unwrap();
        let ref mut w = BufWriter::new(file);

        let mut encoder = png::Encoder::new(w, self.width, self.height);
        encoder.set_color(png::ColorType::RGB);
        encoder.set_depth(png::BitDepth::Eight);
        let mut writer = encoder.write_header().unwrap();
        writer.write_image_data(&self.data).unwrap();
    }

    pub fn get_width(&self) -> u32 {
        self.width
    }

    pub fn get_height(&self) -> u32 {
        self.height
    }
}
