// https://www.researchgate.net/publication/271196271_The_Low-Rank_LDLT_Quartic_Solver
// This code was adapted from the fortran code attached to the publication above.
// It could easily be modified to find imaginary solutions as well, but because rust
// does not natively have a way of representing them, I'm just returning NAN in those
// cases.

pub fn quartic(a: f64, b: f64, c: f64, d: f64) -> [f64; 4] {
    let phi0 = cubic_b_shift(a, b, c, d);
    let l1 = a / 2.0;
    let l3 = b / 6.0 + phi0 / 2.0;
    let del2 = c - a * l3;
    let l2 = if d <= 0.0 {
        if del2 == 0.0 {
            0.0
        } else {
            2.0 * (d - l3.powi(2)) / del2
        }
    } else {
        if b - l1.powi(2) - 2.0 * l3 == 0.0 {
            0.0
        } else {
            del2 / (2.0 * (b - l1.powi(2) - 2.0 * l3))
        }
    };
    let d2 = if l2 == 0.0 {
        2.0 * b / 3.0 - phi0 - l1.powi(2)
    } else if a == 0.0 && c == 0.0 {
        b - 2.0 * l3
    } else {
        del2 / (2.0 * l2)
    };

    if d2 <= 0.0 {
        let gamma = (-d2).sqrt();
        let mut aq = l1 + gamma;
        let mut bq = l3 + gamma * l2;
        let mut cq = l1 - gamma;
        let mut dq = l3 - gamma * l2;
        if dq.abs() < bq.abs() {
            dq = d / bq;
        } else if dq.abs() > bq.abs() {
            bq = d / dq;
        }
        ac_fit(a, b, c, &mut aq, bq, &mut cq, dq);
        let roots1 = quadratic(a, b, c, d, aq, bq);
        let roots2 = quadratic(a, b, c, d, cq, dq);
        [roots1[0], roots1[1], roots2[0], roots2[1]]
    } else {
        let gamma = d2.sqrt();
        let acx = complex(l1, gamma);
        let bcx = complex(l3, gamma * l2);
        let cdiskr = acx.powi(2) / 4.0 - bcx;
        let zx1 = -acx / 2.0 + (cdiskr.sqrt());
        let zx2 = -acx / 2.0 - (cdiskr.sqrt());
        let zxmax = if zx1.abs() > zx2.abs() { zx1 } else { zx2 };
        let zxmin = bcx / zxmax;
        [zxmin, conjugate(zxmin), zxmax, conjugate(zxmax)]
    }
}

fn depressed_cubic_root(g: f64, h: f64) -> f64 {
    let mut x0: f64 = 0.0;
    let mut xt0;
    let mut a;
    let mut b;
    let mut c;
    let mut xabs;
    let mut oldabs;

    xabs = 0.0;
    if h == 0.0 {
        x0 = 0.0;
    } else {
        if h < 0.0 {
            let xr = (-h).sqrt();
            x0 = if g > xr {
                -h / g
            } else if g < -xr.powi(2) {
                (-g).sqrt()
            } else {
                xr
            };
        } else if h > 0.0 {
            let xr = h.sqrt();
            x0 = if g > xr {
                -h / g
            } else if g < -xr.powi(2) {
                -(-g).sqrt()
            } else {
                -xr
            };
        }
        for iter in 1..=8 {
            a = x0.powi(2);
            b = -h;
            c = g * x0.powi(2) + 2.0 * h * x0;
            let (xmin, _) = r_quadratic(a, b, c);
            x0 = xmin;
            xt0 = x0;
            a = 2.0 * x0;
            b = g - x0.powi(2);
            c = h;
            let (xmin, xmax) = r_quadratic(a, b, c);
            if h < 0.0 {
                x0 = if xmax >= 0.0 { xmax } else { xmin };
            } else if h > 0.0 {
                x0 = if xmax <= 0.0 { xmax } else { xmin };
            }
            oldabs = xabs;
            xabs = (xt0 - x0).abs();
            if iter > 1 && (xabs == oldabs || xabs == 0.0) {
                return x0;
            }
        }
    }
    x0
}

fn r_quadratic(a: f64, b: f64, c: f64) -> (f64, f64) {
    let diskr = b * b - 4.0 * a * c;
    if diskr < 0.0 {
        (-b / (2.0 * a), -b / (2.0 * a))
    } else {
        let nenn = if b > 0.0 {
            -b - diskr.sqrt()
        } else {
            -b + diskr.sqrt()
        };
        if nenn == 0.0 || a == 0.0 {
            (0.0, 0.0)
        } else {
            (2.0 * c / nenn, nenn / (2.0 * a))
        }
    }
}

fn ac_fit(a: f64, b: f64, c: f64, aq: &mut f64, bq: f64, cq: &mut f64, dq: f64) {
    let mut cr;
    let mut sr;

    if aq.abs() > cq.abs() {
        let mut cmat = [[*aq, b - bq - dq], [bq, c - (*aq) * dq], [1.0, a - *aq]];

        let mut x1 = cmat[0][0];
        let mut x2 = cmat[2][0];
        let mut r = (x1.powi(2) + x2.powi(2)).sqrt();

        if r == 0.0 {
            cr = 1.0;
            sr = 0.0;
        } else {
            if x2.abs() > x1.abs() {
                sr = x2 / r;
                cr = sr * x1 / x2;
            } else {
                cr = x1 / r;
                sr = cr * x2 / x1;
            }
        }
        for j in 0..=1 {
            x1 = cr * cmat[0][j] + sr * cmat[2][j];
            x2 = -sr * cmat[0][j] + cr * cmat[2][j];
            cmat[0][j] = x1;
            cmat[2][j] = x2;
        }
        x1 = cmat[0][0];
        x2 = cmat[1][0];
        r = (x1.powi(2) + x2.powi(2)).sqrt();
        if r == 0.0 {
            cr = 1.0;
            sr = 0.0;
        } else {
            if x2.abs() > x1.abs() {
                sr = x2 / r;
                cr = sr * x1 / x2;
            } else {
                cr = x1 / r;
                sr = cr * x2 / x1;
            }
        }
        for j in 0..=1 {
            x1 = cr * cmat[0][j] + sr * cmat[1][j];
            x2 = -sr * cmat[0][j] + cr * cmat[1][j];
            cmat[0][j] = x1;
            cmat[1][j] = x2;
        }
        *cq = cmat[0][1] / cmat[0][0];
    } else {
        let mut cmat = [[*cq, b - bq - dq], [dq, c - bq * (*cq)], [1.0, a - (*cq)]];

        let mut x1 = cmat[0][0];
        let mut x2 = cmat[2][0];
        let mut r = (x1.powi(2) + x2.powi(2)).sqrt();
        if r == 0.0 {
            cr = 1.0;
            sr = 0.0;
        } else {
            if x2.abs() > x1.abs() {
                sr = x2 / r;
                cr = sr * x1 / x2;
            } else {
                cr = x1 / r;
                sr = cr * x2 / x1;
            }
        }
        for j in 0..=1 {
            x1 = cr * cmat[0][j] + sr * cmat[2][j];
            x2 = -sr * cmat[0][j] + cr * cmat[2][j];
            cmat[0][j] = x1;
            cmat[2][j] = x2;
        }
        x1 = cmat[0][0];
        x2 = cmat[1][0];
        r = (x1.powi(2) + x2.powi(2)).sqrt();
        if r == 0.0 {
            cr = 1.0;
            sr = 0.0;
        } else {
            if x2.abs() > x1.abs() {
                sr = x2 / r;
                cr = sr * x1 / x2;
            } else {
                cr = x1 / r;
                sr = cr * x2 / x1;
            }
        }
        for j in 0..=1 {
            x1 = cr * cmat[0][j] + sr * cmat[1][j];
            x2 = -sr * cmat[0][j] + cr * cmat[1][j];
            cmat[0][j] = x1;
            cmat[1][j] = x2;
        }
        *aq = cmat[0][1] / cmat[0][0];
    }
}

fn quadratic(aa: f64, bb: f64, cc: f64, dd: f64, mut a: f64, mut b: f64) -> [f64; 2] {
    let mut dpar = [0.0; 2];
    let mut at;
    let mut bt;
    let mut err;
    let mut errt;

    let mut evec = [
        bb * (b) - b.powi(2) - (a) * (b) * aa + a.powi(2) * (b) - dd,
        cc * (b) - b.powi(2) * aa + b.powi(2) * (a) - (a) * dd,
    ];
    err = evec[0].abs() + evec[1].abs();
    if err != 0.0 {
        for _ in 1..=8 {
            evec[0] = -evec[0];
            evec[1] = -evec[1];
            let fmat = [
                [
                    -(b) * aa + 2.0 * (a) * (b),
                    bb - 2.0 * (b) - (a) * aa + a.powi(2),
                ],
                [b.powi(2) - dd, cc - 2.0 * (b) * aa + 2.0 * (b) * (a)],
            ];
            two_lin_eqs(fmat, &mut evec, &mut dpar);
            at = a;
            bt = b;
            a += dpar[0];
            b += dpar[1];
            evec = [
                bb * (b) - b.powi(2) - (a) * (b) * aa + a.powi(2) * b - dd,
                cc * (b) - b.powi(2) * aa + b.powi(2) * (a) - (a) * dd,
            ];

            errt = err;
            err = evec[0].abs() + evec[1].abs();
            if err == 0.0 {
                break;
            }
            if err > errt {
                a = at;
                b = bt;
                break;
            }
        }
    }
    let diskr = a.powi(2) - 4.0 * (b);
    if diskr >= 0.0 {
        let div = if a >= 0.0 {
            -a - (diskr).sqrt()
        } else {
            -a + (diskr).sqrt()
        };
        let zmax = div / 2.0;
        let zmin = if zmax == 0.0 { 0.0 } else { b / zmax };
        [complex(zmax, 0.0), complex(zmin, 0.0)]
    } else {
        [
            complex(-a / 2.0, (-diskr).sqrt() / 2.0),
            complex(-a / 2.0, -(-diskr).sqrt() / 2.0),
        ]
    }
}

fn two_lin_eqs(mut fmat: [[f64; 2]; 2], evec: &mut [f64; 2], dalf: &mut [f64; 2]) {
    let r = (fmat[0][0].powi(2) + fmat[1][0].powi(2)).sqrt();
    let (c, s) = if r > 0.0 {
        (fmat[0][0] / r, fmat[1][0] / r)
    } else {
        (1.0, 0.0)
    };
    let mut x1 = c * fmat[0][0] + s * fmat[1][0];
    let mut x2 = -s * fmat[0][0] + c * fmat[1][0];
    fmat[0][0] = x1;
    fmat[1][0] = x2;
    x1 = c * fmat[0][1] + s * fmat[1][1];
    x2 = -s * fmat[0][1] + c * fmat[1][1];
    fmat[0][1] = x1;
    fmat[1][1] = x2;
    x1 = c * evec[0] + s * evec[1];
    x2 = -s * evec[0] + c * evec[1];
    *evec = [x1, x2];
    dalf[1] = if fmat[1][1] == 0.0 {
        0.0
    } else {
        evec[1] / fmat[1][1]
    };
    dalf[0] = if fmat[0][0] == 0.0 {
        0.0
    } else {
        (evec[0] - fmat[0][1] * dalf[1]) / fmat[0][0]
    };
}

fn cubic_b_shift(a: f64, b: f64, c: f64, d: f64) -> f64 {
    let mut diskr = 9.0 * a.powi(2) - 24.0 * b;
    let s = if diskr > 0.0 {
        diskr = diskr.sqrt();
        if a > 0.0 {
            -2.0 * b / (3.0 * a + diskr)
        } else {
            -2.0 * b / (3.0 * a - diskr)
        }
    } else {
        -a / 4.0
    };
    let aq = a + 4.0 * s;
    let bq = b + 3.0 * s * (a + 2.0 * s);
    let cq = c + s * (2.0 * b + s * (3.0 * a + 4.0 * s));
    let dq = d + s * (c + s * (b + s * (a + s)));
    let gg = bq.powi(2) / 9.0;
    let hh = aq * cq;
    let g = hh - 4.0 * dq - 3.0 * gg;
    let h = (8.0 * dq + hh - 2.0 * gg) * bq / 3.0 - cq.powi(2) - dq * aq.powi(2);
    depressed_cubic_root(g, h)
}

fn complex(r: f64, i: f64) -> f64 {
    if i == 0.0 {
        r
    } else {
        f64::NAN
    }
}

fn conjugate(_: f64) -> f64 {
    f64::NAN
}
