pub use quadratic::quadratic;
pub use quartic::quartic;

mod quadratic;
mod quartic;
