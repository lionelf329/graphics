pub fn quadratic(a: f64, b: f64, c: f64) -> Option<f64> {
    let d = b * b - a * c; // This is different to a regular quadratic, which uses 4ac
    if d >= 0.0 {
        let d = d.sqrt();
        let x1 = (-b - d) / a;
        if x1 >= 0.0 {
            return Some(x1);
        }
        let x2 = (-b + d) / a;
        if x2 >= 0.0 {
            return Some(x2);
        }
    }
    None
}
