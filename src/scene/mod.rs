use crate::Point;
pub use camera::Camera;
pub use object::{Color, Material, Mesh, Object, Reflectance, Shape};

mod camera;
mod object;

/// A point light source.
#[derive(Copy, Clone, Debug)]
pub struct LightSource {
    /// The location of the light source, in world coordinates.
    pub position: Point,
    /// The color/intensity of the light, on a scale of 0-1 for each component.
    pub intensity: [f64; 3],
}
