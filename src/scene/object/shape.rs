use crate::solvers::quadratic;
use crate::{Point, Vector};
use std::f64::consts::PI;

/// A primitive shape.
#[derive(Copy, Clone, Debug)]
pub enum Shape {
    /// A circle on the xy plane with a radius of 1.
    Circle,
    /// A cone with a point at the origin and a radius of 1, spanning from 0 to 1 on the z axis.
    Cone,
    /// A cylinder with a radius of 1, spanning from 0 to 1 on the z axis.
    Cylinder,
    /// A 1x1 square on the xy plane with one corner at the origin growing towards +xy.
    Plane,
    /// A sphere with a radius of 1.
    Sphere,
    /// A torus on the xy plane with a major radius of 1 and a configurable minor radius.
    Torus(f64),
}

impl PartialEq for Shape {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Shape::Circle, Shape::Circle) => true,
            (Shape::Cone, Shape::Cone) => true,
            (Shape::Cylinder, Shape::Cylinder) => true,
            (Shape::Plane, Shape::Plane) => true,
            (Shape::Sphere, Shape::Sphere) => true,
            (Shape::Torus(r1), Shape::Torus(r2)) => r1.to_bits() == r2.to_bits(),
            _ => false,
        }
    }
}

impl Eq for Shape {}

static PI_2: f64 = 2.0 * PI;

impl Shape {
    /// Returns a point and an associated normal vector in generic object coordinates.
    pub fn get_point(&self, u: f64, v: f64) -> (Point, Vector) {
        match self {
            Shape::Circle => (
                Point::new(u * v.cos(), u * v.sin(), 0.0),
                Vector::new(0.0, 0.0, 1.0),
            ),
            Shape::Cone => (
                Point::new(u * v.sin(), u * v.cos(), u),
                Vector::new(v.sin(), v.cos(), -1.0),
            ),
            Shape::Cylinder => (
                Point::new(v.sin(), v.cos(), u),
                Vector::new(v.sin(), v.cos(), 0.0),
            ),
            Shape::Plane => (Point::new(u, v, 0.0), Vector::new(0.0, 0.0, 1.0)),
            Shape::Sphere => (
                Point::new(v.cos() * u.sin(), u.sin() * v.sin(), u.cos()),
                Vector::new(v.cos() * u.sin(), u.sin() * v.sin(), u.cos()),
            ),
            Shape::Torus(outer_radius) => (
                Point::new(
                    (1.0 + outer_radius * v.cos()) * u.cos(),
                    (1.0 + outer_radius * v.cos()) * u.sin(),
                    outer_radius * v.sin(),
                ),
                Vector::new(v.cos() * u.cos(), v.cos() * u.sin(), v.sin()),
            ),
        }
    }

    /// Returns the upper bound for the `u` and `v` ranges.
    pub fn get_uv_range(&self) -> (f64, f64) {
        match self {
            Shape::Circle { .. } => (1.0, PI_2),
            Shape::Cone { .. } => (1.0, PI_2),
            Shape::Cylinder { .. } => (1.0, PI_2),
            Shape::Plane { .. } => (1.0, 1.0),
            Shape::Sphere { .. } => (PI, PI_2),
            Shape::Torus { .. } => (PI_2, PI_2),
        }
    }

    /// Returns the distance in generic coordinates of the closest intersection.
    pub fn get_intersection(&self, e: Point, d: Vector) -> Option<f64> {
        match self {
            Shape::Circle => {
                let x = -e[2] / d[2];
                let p = e + d * x;
                if x >= 0.0 && p[0].powi(2) + p[1].powi(2) <= 1.0 {
                    Some(x)
                } else {
                    None
                }
            }
            Shape::Cone => {
                let a = d[0] * d[0] + d[1] * d[1] - d[2] * d[2];
                let b = d[0] * e[0] + d[1] * e[1] - d[2] * e[2];
                let c = e[0] * e[0] + e[1] * e[1] - e[2] * e[2];
                match quadratic(a, b, c) {
                    Some(x) if x >= 0.0 && in_range((e + d * x)[2]) => Some(x),
                    _ => None,
                }
            }
            Shape::Cylinder => {
                let a = d[0] * d[0] + d[1] * d[1];
                let b = d[0] * e[0] + d[1] * e[1];
                let c = e[0] * e[0] + e[1] * e[1] - 1.0;
                match quadratic(a, b, c) {
                    Some(x) if x >= 0.0 && in_range((e + d * x)[2]) => Some(x),
                    _ => None,
                }
            }
            Shape::Plane => {
                let x = -e[2] / d[2];
                let p = e + d * x;
                if x >= 0.0 && in_range(p[0]) && in_range(p[1]) {
                    Some(x)
                } else {
                    None
                }
            }
            Shape::Sphere => quadratic(d.dot(d), d.dot(e), e.dot(e) - 1.0),
            Shape::Torus(r) => {
                // https://marcin-chwedczuk.github.io/ray-tracing-torus
                let dd = d.dot(d);
                let de = d.dot(e);
                let ee = e.dot(e);
                let r2 = r.powi(2);
                let v = ee - r2 - 1.0;

                let x = dd.powi(2);
                let a = (4.0 * dd * de) / x;
                let b = (2.0 * dd * v + 4.0 * (de.powi(2) + d[2].powi(2))) / x;
                let c = (4.0 * v * de + 8.0 * d[2] * e[2]) / x;
                let d = (v.powi(2) - 4.0 * (r2 - e[2].powi(2))) / x;

                crate::solvers::quartic(a, b, c, d)
                    .iter()
                    .filter(|x| **x >= 0.0)
                    .min_by(|a, b| a.partial_cmp(b).unwrap())
                    .cloned()
            }
        }
    }

    /// Returns the normal to the surface at the given generic object coordinates.
    pub fn get_normal(&self, intersection: Point) -> Vector {
        let i = intersection;
        match self {
            Shape::Circle => Vector::new(0.0, 0.0, 1.0),
            Shape::Cone => Vector::new(i[0], i[1], -i[2]),
            Shape::Cylinder => Vector::new(i[0], i[1], 0.0),
            Shape::Plane => Vector::new(0.0, 0.0, 1.0),
            Shape::Sphere => i - Point::new(0.0, 0.0, 0.0),
            Shape::Torus(_) => {
                let (x, y) = (i[0], i[1]);
                let r = (x * x + y * y).sqrt();
                i - Point::new(x / r, y / r, 0.0)
            }
        }
    }
}

fn in_range(n: f64) -> bool {
    0.0 <= n && n <= 1.0
}
