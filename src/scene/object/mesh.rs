use crate::Shape;

/// Geometry information for an object.
#[derive(Copy, Clone, Debug)]
pub struct Mesh {
    /// The primitive shape.
    pub shape: Shape,
    /// The size of the `u` step, on a scale of (0, 1).
    pub du: f64,
    /// The size of the `v` step, on a scale of (0, 1).
    pub dv: f64,
}

impl Mesh {
    /// Returns a `Mesh` constructed from a shape and two step sizes.
    pub fn new(shape: Shape, du: f64, dv: f64) -> Self {
        Mesh { shape, du, dv }
    }

    /// Returns the step and maximum for the `u` and `v` ranges.
    ///
    /// The structure of the return value is `((u_step, u_max), (v_step, v_max))`.
    pub fn get_uv_range(&self) -> ((f64, f64), (f64, f64)) {
        let (u_max, v_max) = self.shape.get_uv_range();
        ((self.du * u_max, u_max), (self.dv * v_max, v_max))
    }
}
