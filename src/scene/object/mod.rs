use crate::{Matrix, Point, Vector};
pub use material::{Color, Material, Reflectance};
pub use mesh::Mesh;
pub use shape::Shape;

mod material;
mod mesh;
mod shape;

/// A geometric object positioned in 3d space.
#[derive(Copy, Clone, Debug)]
pub struct Object {
    /// Geometry information.
    pub mesh: Mesh,
    /// Color and lighting information.
    pub material: Material,
    /// generic to world matrix.
    g2w: Matrix,
    /// world to generic matrix.
    w2g: Matrix,
}

impl Object {
    /// Returns an object constructed from geometric information, material/lighting information, and
    /// a transformation matrix.
    pub fn new(mesh: Mesh, material: Material, transform: Matrix) -> Object {
        Object {
            mesh,
            material,
            g2w: transform,
            w2g: transform.inverse(),
        }
    }

    /// Returns a point and an associated normal vector in world coordinates.
    pub fn get_point(&self, u: f64, v: f64) -> (Point, Vector) {
        let (point, normal) = self.mesh.shape.get_point(u, v);
        (self.g2w * point, self.w2g.transpose() * normal)
    }

    /// Returns the distance in world coordinates of the closest intersection.
    pub fn get_intersection(&self, origin: Point, direction: Vector) -> Option<f64> {
        self.mesh
            .shape
            .get_intersection(self.w2g * origin, self.w2g * direction)
    }

    /// Returns the normal to the surface at the given world coordinates.
    pub fn get_normal(&self, intersection: Point) -> Vector {
        (self.w2g.transpose() * self.mesh.shape.get_normal(self.w2g * intersection)).normalize()
    }

    /// Changes the object's position in world coordinates.
    pub fn set_transform(&mut self, transform: Matrix) {
        self.g2w = transform;
        self.w2g = transform.inverse();
    }
}
