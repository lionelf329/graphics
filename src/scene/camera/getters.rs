use crate::{Camera, Point};

impl Camera {
    /// Returns the location of the camera in 3 dimensional space.
    pub fn position(&self) -> Point {
        self.position
    }

    /// Returns the width of the viewport.
    pub fn width(&self) -> f64 {
        self.width
    }

    /// Returns the height of the viewport.
    pub fn height(&self) -> f64 {
        self.height
    }

    /// Returns the camera's field of view, measured in radians.
    ///
    /// Field of view is specified for the height of the image, not the width. For square images,
    /// the field of view will be the same on both axes, but otherwise this value represents
    /// the angle the camera makes with the top and bottom edges of the screen.
    pub fn fov(&self) -> f64 {
        self.theta
    }
}
