use crate::{Camera, Matrix, Point};
use std::ops::Mul;

impl Camera {
    /// Applies the world-to-viewing transform to the argument and returns the result.
    pub fn world_to_viewing<T, U>(&self, p: T) -> U
    where
        Matrix: Mul<T, Output = U>,
    {
        self.w2v * p
    }

    /// Applies the world-to-image transform to the argument and returns the result.
    pub fn world_to_image<T, U>(&self, p: T) -> U
    where
        Matrix: Mul<T, Output = U>,
    {
        self.w2i * p
    }

    /// Applies the image-to-world transform to the argument and returns the result.
    pub fn image_to_world<T, U>(&self, p: T) -> U
    where
        Matrix: Mul<T, Output = U>,
    {
        self.i2w * p
    }

    /// Applies the viewing-to-image transform to the argument and returns the result.
    pub fn viewing_to_image<T, U>(&self, p: T) -> U
    where
        Matrix: Mul<T, Output = U>,
    {
        self.v2i * p
    }

    /// Returns the screen coordinates associated with a world coordinate.
    pub fn world_to_pixel(&self, p: Point) -> [f64; 2] {
        self.image_to_pixel(self.w2i * p)
    }

    /// Returns the screen coordinates associated with a viewing coordinate.
    pub fn viewing_to_pixel(&self, p: Point) -> [f64; 2] {
        self.image_to_pixel(self.v2i * p)
    }

    /// Returns the screen coordinates associated with an image coordinate.
    pub fn image_to_pixel(&self, p: Point) -> [f64; 2] {
        let inv = -p[2].recip();
        [p[0] * inv, p[1] * inv]
    }
}
