use crate::{Camera, Point, Vector};

impl Camera {
    /// Changes the up direction, position, and gaze point, then recalculates transforms.
    pub fn move_camera(&mut self, u: Vector, e: Point, g: Point) {
        self.up = u;
        self.position = e;
        self.gaze = g;
        self.w2v = Camera::get_world_to_viewing(self.up, self.position, self.gaze);
        self.w2i = self.v2i * self.w2v;
        self.i2w = self.w2i.inverse();
    }

    /// Changes the dimensions of the viewport and the field of view, then recalculates transforms.
    pub fn resize_viewport(&mut self, width: u32, height: u32, theta: f64) {
        self.width = width as f64;
        self.height = height as f64;
        self.theta = theta;
        self.v2i = Camera::get_viewing_to_image(self.width, self.height, self.theta);
        self.w2i = self.v2i * self.w2v;
        self.i2w = self.w2i.inverse();
    }
}
