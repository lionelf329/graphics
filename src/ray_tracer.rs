use crate::lighting::get_color;
use crate::{Camera, Color, LightSource, Object, Point, Vector};

/// Returns the color of a pixel based on the contents of the scene and the pixel coordinates.
pub fn pixel_color(
    objects: &[Object],
    camera: &Camera,
    light: &LightSource,
    x: u32,
    y: u32,
) -> Option<Color> {
    let direction = camera.image_to_world(Vector::new((x + 1) as f64, (y + 1) as f64, -1.0));
    intersection(camera.position(), direction, objects)
        .map(|x| get_face_color(x, direction, camera, objects, light))
}

fn intersection(e: Point, direction: Vector, objects: &[Object]) -> Option<(usize, Point)> {
    let mut intersection: Option<(usize, f64)> = None;
    for (i, object) in objects.iter().enumerate() {
        if let Some(t) = object.get_intersection(e, direction) {
            if intersection.is_none() || intersection.unwrap().1 > t {
                intersection = Some((i, t));
            }
        }
    }
    match intersection {
        Some((i, t)) => Some((i, e + direction * t)),
        None => None,
    }
}

fn get_face_color(
    (i, c): (usize, Point),
    direction: Vector,
    camera: &Camera,
    objects: &[Object],
    light: &LightSource,
) -> Color {
    let object = objects[i];
    let n = object.get_normal(c);
    let s = (light.position - c).normalize();
    let shadowed = shadowed(c, s, objects);
    let n = camera.world_to_viewing(n);
    let s = camera.world_to_viewing(s);
    let v = camera.world_to_viewing(-direction).normalize();
    get_color(n, v, s, light.intensity, &object.material, shadowed).0
}

fn shadowed(mut i: Point, s: Vector, objects: &[Object]) -> bool {
    i += s * 0.001;
    objects
        .into_iter()
        .any(|object| object.get_intersection(i, s).is_some())
}
