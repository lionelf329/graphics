pub use matrix::Matrix;
pub use point::Point;
pub use vector::Vector;

mod matrix;
mod point;
pub mod transform;
mod vector;

/// A marker trait implemented only by `Vector` and `Point`.
///
/// It is used to make the `dot()` method on each type accept both points and vectors. It has no
/// methods, but it must be public because it is used in a public interface.
pub trait VectorOrPoint: private::Sealed + Copy + std::ops::Index<usize, Output = f64> {}

impl VectorOrPoint for Vector {}
impl VectorOrPoint for Point {}

mod private {
    use crate::{Point, Vector};

    pub trait Sealed {}

    impl Sealed for Vector {}
    impl Sealed for Point {}
}
