use crate::{Matrix, Vector};

/// Returns the rotation matrix around the given vector by the given angle, in radians.
pub fn rotate(t: Vector, a: f64) -> Matrix {
    let t = t.normalize();
    let r = Matrix {
        values: [
            [0., -t[2], t[1], 0.],
            [t[2], 0., -t[0], 0.],
            [-t[1], t[0], 0., 0.],
        ],
    };
    let mut ans = Matrix::I;
    let t1 = r * a.sin();
    let t2 = r * r * (1.0 - a.cos());
    for y in 0..3 {
        for x in 0..4 {
            ans[y][x] += t1[y][x] + t2[y][x];
        }
    }
    ans
}

/// Returns the rotation matrix around the x axis by the given angle, in radians.
pub fn rotate_x(a: f64) -> Matrix {
    let cos = a.cos();
    let sin = a.sin();
    Matrix {
        values: [
            [1.0, 0.0, 0.0, 0.0],
            [0.0, cos, -sin, 0.0],
            [0.0, sin, cos, 0.0],
        ],
    }
}

/// Returns the rotation matrix around the y axis by the given angle, in radians.
pub fn rotate_y(a: f64) -> Matrix {
    let cos = a.cos();
    let sin = a.sin();
    Matrix {
        values: [
            [cos, 0.0, sin, 0.0],
            [0.0, 1.0, 0.0, 0.0],
            [-sin, 0.0, cos, 0.0],
        ],
    }
}

/// Returns the rotation matrix around the z axis by the given angle, in radians.
pub fn rotate_z(a: f64) -> Matrix {
    let cos = a.cos();
    let sin = a.sin();
    Matrix {
        values: [
            [cos, -sin, 0.0, 0.0],
            [sin, cos, 0.0, 0.0],
            [0.0, 0.0, 1.0, 0.0],
        ],
    }
}

/// Returns the rotation matrix that turns the `from` vector into the `to` vector.
pub fn rotate_to(from: Vector, to: Vector) -> Matrix {
    rotate(from.cross(to), from.angle(to))
}
