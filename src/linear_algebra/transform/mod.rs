//! A collection of functions for building affine transform matrices.

pub use rotation::*;
pub use scaling::*;
pub use translation::*;

mod rotation;
mod scaling;
mod translation;

#[cfg(test)]
mod _tests;
