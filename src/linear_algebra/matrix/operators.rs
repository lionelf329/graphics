use crate::{Matrix, Point, Vector};
use std::ops::*;

impl Add for Matrix {
    type Output = Self;

    fn add(self, other: Self) -> Self::Output {
        let mut values = self.values;
        for y in 0..3 {
            for x in 0..4 {
                values[y][x] += other[y][x];
            }
        }
        Matrix { values }
    }
}

impl Sub for Matrix {
    type Output = Self;

    fn sub(self, other: Self) -> Self::Output {
        let mut values = self.values;
        for y in 0..3 {
            for x in 0..4 {
                values[y][x] -= other[y][x];
            }
        }
        Matrix { values }
    }
}

impl AddAssign for Matrix {
    fn add_assign(&mut self, other: Self) {
        for y in 0..3 {
            for x in 0..4 {
                self.values[y][x] += other[y][x];
            }
        }
    }
}

impl SubAssign for Matrix {
    fn sub_assign(&mut self, other: Self) {
        for y in 0..3 {
            for x in 0..4 {
                self.values[y][x] -= other[y][x];
            }
        }
    }
}

impl Mul<f64> for Matrix {
    type Output = Self;

    fn mul(self, other: f64) -> Self::Output {
        let mut values = [[0.0; 4]; 3];
        for y in 0..3 {
            for x in 0..4 {
                values[y][x] = self[y][x] * other;
            }
        }
        Matrix { values }
    }
}

impl MulAssign<f64> for Matrix {
    fn mul_assign(&mut self, other: f64) {
        for y in 0..3 {
            for x in 0..4 {
                self[y][x] *= other;
            }
        }
    }
}

impl Mul<Point> for Matrix {
    type Output = Point;

    fn mul(self, other: Point) -> Self::Output {
        let mut values = Point::new(self[0][3], self[1][3], self[2][3]);
        for y in 0..3 {
            for x in 0..3 {
                values[y] += self[y][x] * other[x];
            }
        }
        values
    }
}

impl Mul<Vector> for Matrix {
    type Output = Vector;

    fn mul(self, other: Vector) -> Self::Output {
        let mut values = Vector::new(0.0, 0.0, 0.0);
        for y in 0..3 {
            for x in 0..3 {
                values[y] += self[y][x] * other[x];
            }
        }
        values
    }
}

impl Mul for Matrix {
    type Output = Self;

    fn mul(self, other: Self) -> Self::Output {
        let mut values = [[0.0; 4]; 3];
        for y in 0..3 {
            for x in 0..4 {
                for z in 0..3 {
                    values[y][x] += self[y][z] * other[z][x];
                }
            }
            values[y][3] += self[y][3];
        }
        Matrix { values }
    }
}

impl MulAssign for Matrix {
    fn mul_assign(&mut self, other: Self) {
        *self = *self * other;
    }
}

impl Div<f64> for Matrix {
    type Output = Self;

    fn div(self, other: f64) -> Self::Output {
        let mut values = [[0.0; 4]; 3];
        let reciprocal = other.recip();
        for y in 0..3 {
            for x in 0..4 {
                values[y][x] = self[y][x] * reciprocal;
            }
        }
        Matrix { values }
    }
}

impl DivAssign<f64> for Matrix {
    fn div_assign(&mut self, other: f64) {
        let reciprocal = other.recip();
        for y in 0..3 {
            for x in 0..4 {
                self[y][x] *= reciprocal;
            }
        }
    }
}

impl Index<usize> for Matrix {
    type Output = [f64; 4];

    fn index(&self, other: usize) -> &Self::Output {
        &self.values[other]
    }
}

impl IndexMut<usize> for Matrix {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        &mut self.values[index]
    }
}
