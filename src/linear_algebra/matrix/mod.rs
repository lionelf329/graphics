mod display;
mod operators;
mod transforms;

#[cfg(test)]
mod _tests;

/// An affine transformation in 3 dimensional space.
///
/// This matrix contains a 3x4 array of `f64` values, which represent an affine transformation in
/// 3 dimensional space. In most regards, it behaves as though it were a 4x4 matrix, with the
/// following structure:
/// ```text
/// ┌              ┐
/// │  X  X  X  X  │
/// │  X  X  X  X  │
/// │  X  X  X  X  │
/// │  0  0  0  1  │
/// └              ┘
/// ```
#[derive(Copy, Clone, Debug, PartialEq)]
pub struct Matrix {
    pub(crate) values: [[f64; 4]; 3],
}

impl Matrix {
    /// The 4x4 identity matrix.
    pub const I: Self = Matrix {
        values: [
            [1.0, 0.0, 0.0, 0.0],
            [0.0, 1.0, 0.0, 0.0],
            [0.0, 0.0, 1.0, 0.0],
        ],
    };

    /// Returns a matrix constructed from a 3x4 array of `f64` values.
    pub const fn new(values: [[f64; 4]; 3]) -> Self {
        Self { values }
    }

    /// Returns a new matrix with the 3x3 region in the top left of the grid transposed.
    ///
    /// This transformation has the effect of switching the direction of rotation, while leaving
    /// scaling and translation intact.
    pub fn transpose(&self) -> Matrix {
        Matrix {
            values: [
                [self[0][0], self[1][0], self[2][0], self[0][3]],
                [self[0][1], self[1][1], self[2][1], self[1][3]],
                [self[0][2], self[1][2], self[2][2], self[2][3]],
            ],
        }
    }

    /// Returns the inverse of the matrix.
    pub fn inverse(&self) -> Matrix {
        let m = &self.values;
        let mut v: [[f64; 4]; 3] = [
            [
                m[1][1] * m[2][2] - m[1][2] * m[2][1],
                m[2][1] * m[0][2] - m[2][2] * m[0][1],
                m[0][1] * m[1][2] - m[0][2] * m[1][1],
                0.0,
            ],
            [
                m[1][2] * m[2][0] - m[1][0] * m[2][2],
                m[2][2] * m[0][0] - m[2][0] * m[0][2],
                m[0][2] * m[1][0] - m[0][0] * m[1][2],
                0.0,
            ],
            [
                m[1][0] * m[2][1] - m[1][1] * m[2][0],
                m[2][0] * m[0][1] - m[2][1] * m[0][0],
                m[0][0] * m[1][1] - m[0][1] * m[1][0],
                0.0,
            ],
        ];
        let det_inv = (m[0][0] * v[0][0] + m[0][1] * v[1][0] + m[0][2] * v[2][0]).recip();
        for y in 0..3 {
            v[y][3] = -(m[0][3] * v[y][0] + m[1][3] * v[y][1] + m[2][3] * v[y][2]);
            for x in 0..4 {
                v[y][x] *= det_inv;
            }
        }
        Matrix { values: v }
    }
}
