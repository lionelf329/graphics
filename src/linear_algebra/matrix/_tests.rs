use crate::{Matrix, Point, Vector};

const M1: Matrix = Matrix {
    values: [
        [1.1, 1.2, 1.3, 1.4],
        [2.1, 2.2, 2.3, 2.4],
        [3.1, 3.2, 3.3, 3.4],
    ],
};

const I1: Matrix = Matrix {
    values: [
        [9.0, 8.0, 7.0, 6.0],
        [1.0, 2.0, 3.0, 5.0],
        [1.0, 2.0, 5.0, 7.0],
    ],
};

const I2: Matrix = Matrix {
    values: [
        [0.2, -1.3, 0.5, 1.8],
        [-0.1, 1.9, -1.0, -1.9],
        [0.0, -0.5, 0.5, -1.0],
    ],
};

#[test]
pub fn transpose() {
    let m2 = Matrix {
        values: [
            [1.1, 2.1, 3.1, 1.4],
            [1.2, 2.2, 3.2, 2.4],
            [1.3, 2.3, 3.3, 3.4],
        ],
    };
    assert_eq!(M1.transpose(), m2);
}

#[test]
pub fn inverse() {
    assert_eq!(I1.inverse().to_string(), I2.to_string());
}

#[test]
pub fn to_string() {
    assert_eq!(
        M1.to_string(),
        "[[+1.100, +1.200, +1.300, +1.400]\
        , [+2.100, +2.200, +2.300, +2.400]\
        , [+3.100, +3.200, +3.300, +3.400]]"
    )
}

#[test]
pub fn add_matrix() {
    let m2 = Matrix {
        values: [
            [2.2, 2.4, 2.6, 2.8],
            [4.2, 4.4, 4.6, 4.8],
            [6.2, 6.4, 6.6, 6.8],
        ],
    };
    assert_eq!((M1 + M1).to_string(), m2.to_string());
}

#[test]
pub fn sub_matrix() {
    let m2 = Matrix {
        values: [
            [2.2, 2.4, 2.6, 2.8],
            [4.2, 4.4, 4.6, 4.8],
            [6.2, 6.4, 6.6, 6.8],
        ],
    };
    assert_eq!((m2 - M1).to_string(), M1.to_string());
}

#[test]
pub fn add_assign_matrix() {
    let mut m = M1;
    m += M1;
    let m2 = Matrix {
        values: [
            [2.2, 2.4, 2.6, 2.8],
            [4.2, 4.4, 4.6, 4.8],
            [6.2, 6.4, 6.6, 6.8],
        ],
    };
    assert_eq!(m.to_string(), m2.to_string());
}

#[test]
pub fn sub_assign_matrix() {
    let mut m2 = Matrix {
        values: [
            [2.2, 2.4, 2.6, 2.8],
            [4.2, 4.4, 4.6, 4.8],
            [6.2, 6.4, 6.6, 6.8],
        ],
    };
    m2 -= M1;
    assert_eq!(m2.to_string(), M1.to_string());
}

#[test]
pub fn mul_f64() {
    let m2 = Matrix {
        values: [
            [2.2, 2.4, 2.6, 2.8],
            [4.2, 4.4, 4.6, 4.8],
            [6.2, 6.4, 6.6, 6.8],
        ],
    };
    assert_eq!((M1 * 2.0).to_string(), m2.to_string());
}

#[test]
pub fn mul_assign_f64() {
    let mut m1 = M1;
    let m2 = Matrix {
        values: [
            [2.2, 2.4, 2.6, 2.8],
            [4.2, 4.4, 4.6, 4.8],
            [6.2, 6.4, 6.6, 6.8],
        ],
    };
    m1 *= 2.0;
    assert_eq!(m1.to_string(), m2.to_string());
}

#[test]
pub fn mul_point() {
    let p1 = Point::new(1.0, 2.0, 3.0);
    let p2 = Point::new(8.8, 15.8, 22.8);
    assert_eq!((M1 * p1).to_string(), p2.to_string());
}

#[test]
pub fn mul_vector() {
    let v1 = Vector::new(1.0, 2.0, 3.0);
    let v2 = Vector::new(7.4, 13.4, 19.4);
    assert_eq!((M1 * v1).to_string(), v2.to_string());
}

#[test]
pub fn mul_matrix() {
    assert_eq!((I1 * I2).to_string(), Matrix::I.to_string());
}

#[test]
pub fn mul_assign_matrix() {
    let mut m1 = I1;
    m1 *= I2;
    assert_eq!(m1.to_string(), Matrix::I.to_string());
}

#[test]
pub fn div_f64() {
    let m2 = Matrix {
        values: [
            [2.2, 2.4, 2.6, 2.8],
            [4.2, 4.4, 4.6, 4.8],
            [6.2, 6.4, 6.6, 6.8],
        ],
    };
    assert_eq!((M1 / 0.5).to_string(), m2.to_string());
}

#[test]
pub fn div_assign_f64() {
    let mut m1 = M1;
    let m2 = Matrix {
        values: [
            [2.2, 2.4, 2.6, 2.8],
            [4.2, 4.4, 4.6, 4.8],
            [6.2, 6.4, 6.6, 6.8],
        ],
    };
    m1 /= 0.5;
    assert_eq!(m1.to_string(), m2.to_string());
}

#[test]
pub fn index() {
    let mut m = M1;
    assert_eq!(&m[2][3], &3.4);
    assert_eq!(&mut m[2][3], &mut 3.4);
}
