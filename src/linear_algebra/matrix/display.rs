use crate::Matrix;
use std::fmt::{Display, Formatter, Result, Write};

impl Display for Matrix {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        f.write_char('[')?;
        for y in 0..self.values.len() {
            if y > 0 {
                f.write_str(", ")?;
            }
            f.write_char('[')?;
            for x in 0..self.values[y].len() {
                if x > 0 {
                    f.write_str(", ")?;
                }
                if -0.0001 < self.values[y][x] && self.values[y][x] < 0.0 {
                    write!(f, "{:+.3}", 0.0)?;
                } else {
                    write!(f, "{:+.3}", self.values[y][x])?;
                }
            }
            f.write_char(']')?;
        }
        f.write_char(']')?;
        Ok(())
    }
}
