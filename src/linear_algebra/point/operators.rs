use crate::{Point, Vector};
use std::ops::*;

impl Add<Vector> for Point {
    type Output = Self;

    fn add(self, other: Vector) -> Self::Output {
        Point {
            values: [self[0] + other[0], self[1] + other[1], self[2] + other[2]],
        }
    }
}

impl Sub<Point> for Point {
    type Output = Vector;

    fn sub(self, other: Point) -> Self::Output {
        Vector {
            values: [self[0] - other[0], self[1] - other[1], self[2] - other[2]],
        }
    }
}

impl Sub<Vector> for Point {
    type Output = Self;

    fn sub(self, other: Vector) -> Self::Output {
        Point {
            values: [self[0] - other[0], self[1] - other[1], self[2] - other[2]],
        }
    }
}

impl AddAssign<Vector> for Point {
    fn add_assign(&mut self, other: Vector) {
        self.values[0] += other[0];
        self.values[1] += other[1];
        self.values[2] += other[2];
    }
}

impl SubAssign<Vector> for Point {
    fn sub_assign(&mut self, other: Vector) {
        self.values[0] -= other[0];
        self.values[1] -= other[1];
        self.values[2] -= other[2];
    }
}

impl Index<usize> for Point {
    type Output = f64;

    fn index(&self, index: usize) -> &Self::Output {
        &self.values[index]
    }
}

impl IndexMut<usize> for Point {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        &mut self.values[index]
    }
}
