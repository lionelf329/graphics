use crate::Point;
use std::fmt::{Display, Formatter, Result};

impl Display for Point {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        write!(
            f,
            "({:+.3}, {:+.3}, {:+.3}, {:+.3})",
            self[0], self[1], self[2], 1.0
        )
    }
}
