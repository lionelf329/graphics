use crate::{Point, VectorOrPoint};

mod display;
mod operators;

#[cfg(test)]
mod _tests;

/// A direction in 3 dimensional space.
///
/// This struct contains an array of 3 `f64` values, which represent a direction in 3 dimensional
/// space. In most regards, it behaves as though it were a vector with 4 components, with the
/// following structure:
/// ```text
/// [X Y Z 0]
/// ```
#[derive(Copy, Clone, Debug, PartialEq)]
pub struct Vector {
    pub(crate) values: [f64; 3],
}

impl Vector {
    /// The zero vector.
    pub const ZERO: Self = Self::new(0.0, 0.0, 0.0);

    /// Returns a `Vector` constructed from x, y, and z coordinates.
    pub const fn new(x: f64, y: f64, z: f64) -> Self {
        Self { values: [x, y, z] }
    }

    /// Zero-cost conversion to a point.
    ///
    /// This is equivalent to `Point::ORIGIN + self`.
    pub const fn to_point(self) -> Point {
        Point {
            values: self.values,
        }
    }

    /// Returns the dot product between `self` and a `Point` or `Vector`.
    ///
    /// It uses only the first three components to compute the dot product, the 4th is ignored.
    pub fn dot<T: VectorOrPoint>(&self, other: T) -> f64 {
        self[0] * other[0] + self[1] * other[1] + self[2] * other[2]
    }

    /// Returns the magnitude of the vector.
    pub fn norm(&self) -> f64 {
        (self[0] * self[0] + self[1] * self[1] + self[2] * self[2]).sqrt()
    }

    /// Returns the same vector, rescaled to have a magnitude of 1.
    pub fn normalize(&self) -> Self {
        let norm_inv = self.norm().recip();
        Self {
            values: [self[0] * norm_inv, self[1] * norm_inv, self[2] * norm_inv],
        }
    }

    /// Returns the cross product `self × other`.
    pub fn cross(&self, other: Self) -> Self {
        Self {
            values: [
                self[1] * other[2] - self[2] * other[1],
                self[2] * other[0] - self[0] * other[2],
                self[0] * other[1] - self[1] * other[0],
            ],
        }
    }

    /// Returns the angle between `self` and `other`.
    pub fn angle(&self, other: Self) -> f64 {
        self.normalize().dot(other.normalize()).acos()
    }
}
