use crate::{Point, Vector};
use std::ops::*;

impl Neg for Vector {
    type Output = Self;

    fn neg(self) -> Self::Output {
        Vector {
            values: [-self[0], -self[1], -self[2]],
        }
    }
}

impl Add<Point> for Vector {
    type Output = Point;

    fn add(self, other: Point) -> Self::Output {
        Point {
            values: [self[0] + other[0], self[1] + other[1], self[2] + other[2]],
        }
    }
}

impl Add<Vector> for Vector {
    type Output = Self;

    fn add(self, other: Vector) -> Self::Output {
        Vector {
            values: [self[0] + other[0], self[1] + other[1], self[2] + other[2]],
        }
    }
}

impl Sub<Vector> for Vector {
    type Output = Self;

    fn sub(self, other: Vector) -> Self::Output {
        Vector {
            values: [self[0] - other[0], self[1] - other[1], self[2] - other[2]],
        }
    }
}

impl AddAssign<Vector> for Vector {
    fn add_assign(&mut self, other: Self) {
        self[0] += other[0];
        self[1] += other[1];
        self[2] += other[2];
    }
}

impl SubAssign<Vector> for Vector {
    fn sub_assign(&mut self, other: Self) {
        self[0] -= other[0];
        self[1] -= other[1];
        self[2] -= other[2];
    }
}

impl Mul<f64> for Vector {
    type Output = Self;

    fn mul(self, other: f64) -> Self::Output {
        Vector {
            values: [self[0] * other, self[1] * other, self[2] * other],
        }
    }
}

impl MulAssign<f64> for Vector {
    fn mul_assign(&mut self, other: f64) {
        self[0] *= other;
        self[1] *= other;
        self[2] *= other;
    }
}

impl Div<f64> for Vector {
    type Output = Self;

    fn div(self, other: f64) -> Self::Output {
        self * other.recip()
    }
}

impl DivAssign<f64> for Vector {
    fn div_assign(&mut self, other: f64) {
        self.mul_assign(other.recip());
    }
}

impl Index<usize> for Vector {
    type Output = f64;

    fn index(&self, index: usize) -> &Self::Output {
        &self.values[index]
    }
}

impl IndexMut<usize> for Vector {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        &mut self.values[index]
    }
}
