use crate::{Point, Vector};

#[test]
fn dot() {
    let v1 = Vector::new(1.0, 2.0, 3.0);
    let p = Point::new(4.0, 5.0, 6.0);
    let v = Vector::new(4.0, 5.0, 6.0);
    assert_eq!(v1.dot(p), 32.0);
    assert_eq!(v1.dot(v), 32.0);
}

#[test]
fn norm() {
    let v = Vector::new(3.0, 4.0, 5.0);
    assert_eq!(v.norm(), 50.0_f64.sqrt())
}

#[test]
fn normalize() {
    let v = Vector::new(3.0, 4.0, 5.0);
    assert_eq!(
        v.normalize().to_string(),
        Vector::new(0.424, 0.566, 0.707).to_string(),
    )
}

#[test]
fn cross() {
    let v1 = Vector::new(1.0, 2.0, 3.0);
    let v2 = Vector::new(2.0, 3.0, 1.0);
    assert_eq!(
        v1.cross(v2).to_string(),
        Vector::new(-7.0, 5.0, -1.0).to_string()
    )
}

#[test]
fn angle() {
    let v1 = Vector::new(1.0, 2.0, 3.0);
    let v2 = Vector::new(2.0, 3.0, 1.0);
    assert_eq!(
        format!("{:.8}", v1.angle(v2)),
        format!("{:.8}", (11.0_f64 / 14.0).acos())
    )
}

#[test]
pub fn to_string() {
    assert_eq!(
        Vector::new(1.0, 2.0, 3.0).to_string(),
        "(+1.000, +2.000, +3.000, +0.000)"
    )
}

#[test]
pub fn neg() {
    let v = Vector::new(1.0, 2.0, 3.0);
    assert_eq!((-v).to_string(), Vector::new(-1.0, -2.0, -3.0).to_string(),)
}

#[test]
pub fn add_point() {
    let v = Vector::new(1.0, 3.0, 5.0);
    let p = Point::new(1.0, 2.0, 3.0);
    assert_eq!((v + p).to_string(), Point::new(2.0, 5.0, 8.0).to_string());
}

#[test]
pub fn add_vector() {
    let v1 = Vector::new(1.0, 3.0, 5.0);
    let v2 = Vector::new(1.0, 2.0, 3.0);
    assert_eq!(
        (v1 + v2).to_string(),
        Vector::new(2.0, 5.0, 8.0).to_string()
    );
}

#[test]
pub fn sub_vector() {
    let v1 = Vector::new(1.0, 3.0, 5.0);
    let v2 = Vector::new(1.0, 2.0, 3.0);
    assert_eq!(
        (v1 - v2).to_string(),
        Vector::new(0.0, 1.0, 2.0).to_string()
    );
}

#[test]
pub fn add_assign_vector() {
    let mut v = Vector::new(1.0, 3.0, 5.0);
    v += Vector::new(1.0, 2.0, 3.0);
    assert_eq!(v.to_string(), Vector::new(2.0, 5.0, 8.0).to_string());
}

#[test]
pub fn sub_assign_vector() {
    let mut v = Vector::new(1.0, 3.0, 5.0);
    v -= Vector::new(1.0, 2.0, 3.0);
    assert_eq!(v.to_string(), Vector::new(0.0, 1.0, 2.0).to_string());
}

#[test]
pub fn mul_f64() {
    let v1 = Vector::new(1.0, 2.0, 3.0);
    let v2 = Vector::new(2.0, 4.0, 6.0);
    assert_eq!((v1 * 2.0).to_string(), v2.to_string());
}

#[test]
pub fn mul_assign_f64() {
    let mut v1 = Vector::new(1.0, 2.0, 3.0);
    let v2 = Vector::new(2.0, 4.0, 6.0);
    v1 *= 2.0;
    assert_eq!(v1.to_string(), v2.to_string());
}

#[test]
pub fn div_f64() {
    let v1 = Vector::new(1.0, 2.0, 3.0);
    let v2 = Vector::new(2.0, 4.0, 6.0);
    assert_eq!((v1 / 0.5).to_string(), v2.to_string());
}

#[test]
pub fn div_assign_f64() {
    let mut v1 = Vector::new(1.0, 2.0, 3.0);
    let v2 = Vector::new(2.0, 4.0, 6.0);
    v1 /= 0.5;
    assert_eq!(v1.to_string(), v2.to_string());
}

#[test]
pub fn index() {
    let mut v = Vector::new(1.0, 2.0, 3.0);
    assert_eq!(&v[2], &3.0);
    assert_eq!(&mut v[2], &mut 3.0);
}
