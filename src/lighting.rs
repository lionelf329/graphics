use crate::{Color, Material, Reflectance, Vector};

/// n is the vector from the point that is normal to the shape. It must already be normalized.
/// v is the vector from the point towards the camera. It must already be normalized.
/// s is the vector from the point towards the light source. It must already be normalized.
/// intensity is the light color/intensity.
/// material is the material of the object.
/// shadowed decides whether to use specular and diffuse lighting, or just ambient
pub fn get_color(
    n: Vector,
    v: Vector,
    s: Vector,
    intensity: [f64; 3],
    material: &Material,
    shadowed: bool,
) -> (Color, bool) {
    assert!((n.norm() - 1.0).abs() < 0.00001);
    assert!((v.norm() - 1.0).abs() < 0.00001);
    assert!((s.norm() - 1.0).abs() < 0.00001);
    // r is the vector from the point towards where the light would reflect
    let r = ((n * (s.dot(n) * 2.0)) - s).normalize();

    // coefficients for ambient, diffuse, and specular light, and the exponent for specular light
    let Reflectance {
        ambient: pa,
        diffuse: pd,
        specular: ps,
        specular_exponent: f,
    } = material.reflectance;

    // diffuse light value
    let id = pd * s.dot(n).max(0.0);
    // specular light value
    let is = ps * r.dot(v).max(0.0).powf(f);

    let lighting = if shadowed { pa } else { pa + id + is };

    let color = [
        to_u8(material.color[0] as f64 * intensity[0] * lighting),
        to_u8(material.color[1] as f64 * intensity[1] * lighting),
        to_u8(material.color[2] as f64 * intensity[2] * lighting),
    ];
    (color, v.dot(n) >= 0.0)
}

fn to_u8(f: f64) -> u8 {
    f.clamp(0.0, 255.0) as u8
}
